### Start website server
Server will run on port 8000.
```sh
python3 server4spa.py
```

### Compile Elm code
```sh
elm make --output=app.js src/Main.elm
```

### Enter development shell
Follow your distribution's instructions to install Nix package manager, then run the following command:
```sh
nix-shell
```
