let
nixpkgs = import (builtins.fetchTarball https://github.com/NixOS/nixpkgs/archive/nixos-23.05.tar.gz) {
  overlays = [];
  config = {};
};

in
with nixpkgs;


stdenv.mkDerivation {
  name = "elm-env";
  buildInputs = [];

  nativeBuildInputs = [
    fish
    fzf

    elmPackages.elm
    elmPackages.elm-format
    elmPackages.elm-test
    elmPackages.elm-live
  ];
}
