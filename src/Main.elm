module Main exposing (main)

import Browser
import Browser.Navigation as Nav
import DeviceModels
import Html exposing (..)
import Html.Attributes exposing (classList, href)
import Html.Lazy exposing (lazy)
import Issues
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser, s, string)



-- URL PARSING


urlParser : Parser (Route -> a) a
urlParser =
    Parser.oneOf
        [ Parser.map DeviceModels (Parser.s "device-models")
        , Parser.map Issues (Parser.s "issues")
        ]


updateUrl : Url -> Model -> ( Model, Cmd Msg )
updateUrl url model =
    case Parser.parse urlParser url of
        Just DeviceModels ->
            toDeviceModels model DeviceModels.init

        Just Issues ->
            toIssues model Issues.init

        Nothing ->
            ( { model | page = NotFound }, Cmd.none )



-- VIEW


view : Model -> Browser.Document Msg
view model =
    let
        content =
            case model.page of
                DeviceModelsPage deviceModels ->
                    DeviceModels.view deviceModels
                        |> Html.map GotDeviceModelsMsg

                IssuesPage issues ->
                    Issues.view issues
                        |> Html.map GotIssuesMsg

                NotFound ->
                    text "Page not found."
    in
    { title = "Build Health Status Tracker"
    , body =
        [ lazy viewHeader model.page
        , content
        ]
    }


viewHeader : Page -> Html Msg
viewHeader page =
    let
        logo =
            h1 [] [ text "Build Health Status Tracker" ]

        links =
            ul []
                [ navLink Issues { url = "/issues", caption = "Issues" }
                , navLink DeviceModels { url = "/device-models", caption = "Device Models" }
                ]

        navLink : Route -> { url : String, caption : String } -> Html Msg
        navLink route { url, caption } =
            li
                [ classList [ ( "active", isActive { link = route, page = page } ) ]
                ]
                [ a [ href url ] [ text caption ]
                ]
    in
    nav [] [ logo, links ]


isActive : { link : Route, page : Page } -> Bool
isActive { link, page } =
    case ( link, page ) of
        ( DeviceModels, DeviceModelsPage _ ) ->
            True

        ( DeviceModels, _ ) ->
            False

        ( Issues, IssuesPage _ ) ->
            True

        ( Issues, _ ) ->
            False



-- UPDATE


type Msg
    = ClickedLink Browser.UrlRequest
    | ChangedUrl Url
    | GotDeviceModelsMsg DeviceModels.Msg
    | GotIssuesMsg Issues.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ClickedLink (Browser.External href) ->
            ( model, Nav.load href )

        ClickedLink (Browser.Internal url) ->
            ( model, Nav.pushUrl model.key (Url.toString url) )

        ChangedUrl url ->
            updateUrl url model

        GotDeviceModelsMsg deviceModelsMsg ->
            case model.page of
                DeviceModelsPage deviceModels ->
                    toDeviceModels model (DeviceModels.update deviceModelsMsg deviceModels)

                _ ->
                    ( model, Cmd.none )

        GotIssuesMsg issuesMsg ->
            case model.page of
                IssuesPage issuesPage ->
                    toIssues model (Issues.update issuesMsg issuesPage)

                _ ->
                    ( model, Cmd.none )


toDeviceModels : Model -> ( DeviceModels.Model, Cmd DeviceModels.Msg ) -> ( Model, Cmd Msg )
toDeviceModels model ( deviceModels, cmd ) =
    ( { model | page = DeviceModelsPage deviceModels }, Cmd.map GotDeviceModelsMsg cmd )


toIssues : Model -> ( Issues.Model, Cmd Issues.Msg ) -> ( Model, Cmd Msg )
toIssues model ( issues, cmd ) =
    ( { model | page = IssuesPage issues }, Cmd.map GotIssuesMsg cmd )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- MODEL


type Page
    = DeviceModelsPage DeviceModels.Model
    | IssuesPage Issues.Model
    | NotFound


type Route
    = DeviceModels
    | Issues


type alias Model =
    { page : Page, key : Nav.Key }


init : () -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    updateUrl url { page = NotFound, key = key }



-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = ChangedUrl
        , onUrlRequest = ClickedLink
        , subscriptions = \_ -> Sub.none
        , update = update
        , view = view
        }
