module Utils exposing (..)

import Html exposing (..)
import Html.Attributes exposing (class, src)



-- CONNECTION SETTINGS


serverAddressPrefix : String
serverAddressPrefix =
    "http://127.0.0.1:5000"



-- UI


viewSpinner =
    img
        [ src "https://upload.wikimedia.org/wikipedia/commons/a/a3/Lightness_rotate_36f_cw.gif"

        --   src "https://gifcities.org/assets/loading1.gif"
        , class "spinner"
        ]
        []


viewUnderConstruction =
    div [ class "content" ]
        [ img [ src "http://textfiles.com/underconstruction/ajaj0077lines_bulletsconstruction.gif" ] []
        , img [ src "http://textfiles.com/underconstruction/YosemiteRapids2787constconstruction.gif" ] []
        , img [ src "http://textfiles.com/underconstruction/HeHeartlandEstates8832underconstruction.gif" ] []
        , img [ src "http://textfiles.com/underconstruction/ajaj0077lines_bulletsconstruction.gif" ] []
        ]
