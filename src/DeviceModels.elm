module DeviceModels exposing (Model, Msg, deviceModelDecoder, deviceModelsDataDecoder, directDeviceModelsDecoder, init, update, view)

import Browser
import Html exposing (..)
import Html.Attributes exposing (class, classList, disabled, value)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode exposing (Decoder, bool, int, list, string, succeed)
import Json.Decode.Pipeline exposing (optional, required)
import Json.Encode as Encode
import Platform.Cmd as Cmd
import Utils exposing (serverAddressPrefix, viewSpinner)



-- VIEW


view : Model -> Html Msg
view model =
    div [ class "content" ]
        (h2 [] [ text "Device models" ]
            :: (case model.status of
                    Loaded ->
                        [ viewDeviceModelsSection model.deviceModels
                        , viewAddDeviceModelSection model "Add new device model"
                        , viewDeleteDeviceModelSection model "Delete device model"
                        ]

                    Loading ->
                        [ viewSpinner ]

                    Errored errorMessage ->
                        [ label [ class "label-warning" ]
                            [ text
                                ("An error happened when fetching data from server: "
                                    ++ errorMessage
                                )
                            ]
                        , label [] [ text ("Current device models, for debug purposes: " ++ Debug.toString model.deviceModels) ]
                        ]
               )
        )


viewDeviceModelsSection : List RetrievedDeviceModel -> Html Msg
viewDeviceModelsSection deviceModels =
    div [ class "display-device-models" ]
        [ table [] <|
            [ th [] [ text "Device model name" ]
            , th [] [ text "Is deprecated?" ]
            ]
                ++ List.map
                    viewDeviceModel
                    deviceModels
        ]


viewAddDeviceModelSection : Model -> String -> Html Msg
viewAddDeviceModelSection model sectionName =
    let
        canAddNewDeviceModel =
            model.deviceModelToAdd
                /= ""
                && not (List.member model.deviceModelToAdd (List.map .name model.deviceModels))
    in
    div [ class "add-device-model" ]
        [ h2 [] [ text sectionName ]
        , div [ class "horizontal-div" ]
            [ input
                [ classList
                    [ ( "input-invalid"
                      , not canAddNewDeviceModel && (model.deviceModelToAdd /= "")
                      )
                    ]
                , onInput AddDeviceInputChanged
                , value model.deviceModelToAdd
                ]
                []
            , button
                [ class "confirmation-button"
                , onClick ClickedAddDeviceModel
                , disabled (not canAddNewDeviceModel)
                ]
                [ text "Add device" ]
            ]
        , if not canAddNewDeviceModel && (model.deviceModelToAdd /= "") then
            label [ class "label-warning" ] [ text "Device model with this name already exists." ]

          else
            text ""
        ]


viewDeleteDeviceModelSection : Model -> String -> Html Msg
viewDeleteDeviceModelSection model sectionName =
    let
        canDeleteDeviceModel =
            List.member model.deviceModelToDelete (List.map .name model.deviceModels)
    in
    div [ class "delete-device-model" ]
        [ h2 [] [ text sectionName ]
        , input
            [ classList [ ( "input-invalid", not canDeleteDeviceModel && (model.deviceModelToDelete /= "") ) ]
            , onInput DeleteDeviceInputChanged
            , value model.deviceModelToDelete
            ]
            []
        , button
            [ class "confirmation-button"
            , onClick ClickedDeleteDeviceModel
            , disabled (not canDeleteDeviceModel)
            ]
            [ text "Delete device" ]
        , if not canDeleteDeviceModel && (model.deviceModelToDelete /= "") then
            label [ class "label-warning" ] [ text "Device model with this name does not exist." ]

          else
            text ""
        ]


viewDeviceModel : RetrievedDeviceModel -> Html Msg
viewDeviceModel deviceModel =
    tr [ class "device-model-tr" ]
        [ td [] [ text deviceModel.name ]
        , td []
            [ text <|
                if deviceModel.is_deprecated then
                    "Yes"

                else
                    "No"
            ]
        ]



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AddDeviceInputChanged input ->
            ( { model | deviceModelToAdd = input }, Cmd.none )

        DeleteDeviceInputChanged input ->
            ( { model | deviceModelToDelete = input }, Cmd.none )

        GotDeviceModels (Ok deviceModels) ->
            ( { model
                | status = Loaded
                , deviceModels = deviceModels
              }
            , Cmd.none
            )

        GotDeviceModels (Err httpError) ->
            ( { model | status = Errored (Debug.toString httpError) }, Cmd.none )

        ClickedAddDeviceModel ->
            ( model
            , fetchDeviceModelsCmdWithVariableMsg AddDeviceModel
            )

        ClickedDeleteDeviceModel ->
            ( model, fetchDeviceModelsCmdWithVariableMsg DeleteDeviceModel )

        DeviceModelAdded (Ok _) ->
            ( { model | deviceModelToAdd = "" }, fetchDeviceModelsCmd )

        DeviceModelAdded (Err error) ->
            ( { model | status = Errored (Debug.toString error) }, Cmd.none )

        DeviceModelDeleted (Ok _) ->
            ( { model | deviceModelToDelete = "" }, fetchDeviceModelsCmd )

        DeviceModelDeleted (Err error) ->
            ( { model | status = Errored (Debug.toString error) }, Cmd.none )

        AddDeviceModel (Ok deviceModels) ->
            ( { model | deviceModels = deviceModels }
            , if List.member model.deviceModelToAdd (List.map .name deviceModels) then
                -- no Cmd here, on model update duplicate device will be marked in UI
                Cmd.none

              else
                addNewDeviceModelCmd (buildDeviceModel model.deviceModelToAdd)
            )

        AddDeviceModel (Err httpError) ->
            ( { model | status = Errored (Debug.toString httpError) }, Cmd.none )

        DeleteDeviceModel (Err httpError) ->
            ( { model | status = Errored (Debug.toString httpError) }, Cmd.none )

        DeleteDeviceModel (Ok deviceModels) ->
            let
                deviceModelsToDelete =
                    List.filter (\d -> d.name == model.deviceModelToDelete) model.deviceModels

                deviceIdToDelete =
                    case deviceModelsToDelete of
                        x :: [] ->
                            Just x.device_model_id

                        _ ->
                            Nothing

                cmd =
                    case deviceIdToDelete of
                        Just id ->
                            deleteDeviceModelCmd id

                        Nothing ->
                            Cmd.none
            in
            ( { model | deviceModels = deviceModels }
            , cmd
            )

        DeletedDeviceModel (Err httpError) ->
            ( { model | status = Errored (Debug.toString httpError) }, Cmd.none )

        DeletedDeviceModel (Ok someString) ->
            ( { model | deviceModelToDelete = "" }, fetchDeviceModelsCmd )


type Msg
    = AddDeviceInputChanged String
    | DeleteDeviceInputChanged String
    | GotDeviceModels (Result Http.Error (List RetrievedDeviceModel))
    | DeviceModelAdded (Result Http.Error RetrievedDeviceModel)
    | DeviceModelDeleted (Result Http.Error RetrievedDeviceModel)
    | ClickedAddDeviceModel
    | AddDeviceModel (Result Http.Error (List RetrievedDeviceModel))
    | DeleteDeviceModel (Result Http.Error (List RetrievedDeviceModel))
    | ClickedDeleteDeviceModel
    | DeletedDeviceModel (Result Http.Error String)



-- JSON DECODING AND ENCODING


deviceModelDecoder : Decoder RetrievedDeviceModel
deviceModelDecoder =
    succeed buildDeviceModelWithID
        |> required "id" int
        |> required "name" string
        |> optional "is_deprecated" bool False


deviceModelsDataDecoder : Decoder RetrievedDeviceModelsData
deviceModelsDataDecoder =
    -- This decoder is not used, but is kept as a code reference.
    succeed (\data -> { data = data })
        |> required "data" (list deviceModelDecoder)


directDeviceModelsDecoder : Decoder (List RetrievedDeviceModel)
directDeviceModelsDecoder =
    -- extracts list under "data" key and converts it into list of devices.
    succeed (\data -> data)
        |> required "data" (list deviceModelDecoder)


buildDeviceModel : String -> DeviceModel
buildDeviceModel name =
    { name = name, is_deprecated = False }


buildDeviceModelWithID : Int -> String -> Bool -> RetrievedDeviceModel
buildDeviceModelWithID device_model_id name is_deprecated =
    { device_model_id = device_model_id, name = name, is_deprecated = is_deprecated }


buildDeviceModelWithoutID : Bool -> String -> DeviceModel
buildDeviceModelWithoutID is_deprecated name =
    { name = name, is_deprecated = is_deprecated }


newDeviceModelEncoder : DeviceModel -> Encode.Value
newDeviceModelEncoder deviceModel =
    Encode.object
        [ ( "name", Encode.string deviceModel.name ) ]



-- MODEL


type alias DeviceModel =
    { name : String, is_deprecated : Bool }


type alias RetrievedDeviceModel =
    -- Only difference from the DeviceModel is extra field ID, generated by server.
    { device_model_id : Int, name : String, is_deprecated : Bool }


type alias RetrievedDeviceModelsData =
    { data : List RetrievedDeviceModel }


type Status
    = Loading
    | Loaded
    | Errored String


type alias Model =
    { deviceModels : List RetrievedDeviceModel
    , deviceModelToAdd : String
    , deviceModelToDelete : String
    , deviceModelToAddAsARecord : Bool -> String -> DeviceModel
    , status : Status
    }


init : ( Model, Cmd Msg )
init =
    ( { deviceModels = []
      , deviceModelToAdd = ""
      , deviceModelToDelete = ""
      , deviceModelToAddAsARecord = \_ -> buildDeviceModelWithoutID False
      , status = Loading
      }
    , fetchDeviceModelsCmd
    )



-- COMMANDS


fetchDeviceModelsCmd : Cmd Msg
fetchDeviceModelsCmd =
    Http.get
        { url = serverAddressPrefix ++ "/device_models"
        , expect = Http.expectJson GotDeviceModels directDeviceModelsDecoder
        }


fetchDeviceModelsCmdWithVariableMsg : (Result Http.Error (List RetrievedDeviceModel) -> Msg) -> Cmd Msg
fetchDeviceModelsCmdWithVariableMsg msg =
    Http.get
        { url = serverAddressPrefix ++ "/device_models"
        , expect = Http.expectJson msg directDeviceModelsDecoder
        }


addNewDeviceModelCmd : DeviceModel -> Cmd Msg
addNewDeviceModelCmd deviceModel =
    Debug.log "Posting new device to the server"
        Http.post
        { url = serverAddressPrefix ++ "/device_models"
        , body = Http.jsonBody (newDeviceModelEncoder deviceModel)
        , expect = Http.expectJson DeviceModelAdded deviceModelDecoder
        }


deleteDeviceModelCmd : Int -> Cmd Msg
deleteDeviceModelCmd deviceModelId =
    Http.request
        { method = "DELETE"
        , headers = []
        , timeout = Nothing
        , tracker = Nothing
        , url = serverAddressPrefix ++ "/device_model/" ++ String.fromInt deviceModelId
        , body = Http.emptyBody
        , expect = Http.expectString DeletedDeviceModel
        }



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = \_ -> init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }
