module Issues exposing (Model, Msg, init, update, view)

import Browser
import Element.Font exposing (medium)
import Html exposing (Html, button, div, h1, h2, h3, input, label, option, select, table, td, text, textarea, th, tr)
import Html.Attributes exposing (class, placeholder, type_, value)
import Html.Events exposing (onInput)
import Multiselect
import Utils exposing (viewUnderConstruction)



-- VIEW


view : Model -> Html Msg
view model =
    div [ class "content" ]
        [ h2 [] [ text "Issues" ]
        , div []
            [ viewCurrentIssues model
            , viewAddNewIssue model

            -- , viewEditIssue model
            -- , viewDeleteIssue model
            ]
        ]


viewCurrentIssues : Model -> Html Msg
viewCurrentIssues model =
    div [ class "display-issues" ]
        [ table [] <|
            [ th [] [ text "ID" ]
            , th [] [ text "Priority ℹ️" ]
            , th [] [ text "IssueTracker ID" ]
            , th [] [ text "Affected platforms" ]
            , th [] [ text "Summary" ]
            , th [] [ text "Description" ]
            , th [] [ text "Bug type ℹ️" ]
            ]
                ++ List.map
                    viewIssue
                    model.issues
        ]


viewIssue : Issue -> Html Msg
viewIssue issue =
    tr [ class "issue-tr" ]
        [ td [] [ text (String.fromInt issue.id) ]
        , td [] [ text (priorityToString issue.priority) ]
        , td [] [ text (linkToString issue.link) ]
        , td [] [ text "todo" ]
        , td [] [ text issue.summary ]
        , td []
            [ text <|
                case issue.description of
                    Just desc ->
                        desc

                    Nothing ->
                        ""
            ]
        , td [] [ text (bugTypeToString issue.bugType) ]
        ]


viewAddNewIssue : Model -> Html Msg
viewAddNewIssue model =
    let
        inputs =
            model.addIssueInputs
    in
    div [ class "vertical-div" ]
        [ h2 [] [ text "Add new issue" ]
        , viewInput "text" "IssueTracker ID" inputs.issueTrackerID AddIssueIssueTrackerIDChanged
        , viewSelect "Priority" inputs.priority (List.map .name availablePriorities) AddIssuePriorityChanged
        , viewSelectMultiple "Affected platforms" model
        , viewSelect "Issue type" inputs.issueType (List.map .name availableIssueTypes) AddIssueIssueTypeChanged
        , viewTextArea "Summary" inputs.summary AddIssueSummaryChanged
        , viewTextArea "Description (optional)" inputs.description AddIssueDescriptionChanged
        , button [ class "confirmation-button" ] [ text "Add issue" ]

        -- , label [] [ text (Debug.toString model.availableDevices) ]
        -- , label [] [ text (Debug.toString inputs) ]
        ]


viewEditIssue : Model -> Html Msg
viewEditIssue model =
    div []
        [ h2 [] [ text "Edit issue" ]
        , text "Should display items in the same format as when adding"
        ]


viewDeleteIssue : Model -> Html Msg
viewDeleteIssue model =
    div []
        [ h2 [] [ text "Delete issue" ]
        , viewInput "text" "IssueTracker ID" "" NothingYet
        , button [ class "confirmation-button" ] [ text "Delete issue" ]
        ]


viewInput : String -> String -> String -> (String -> Msg) -> Html Msg
viewInput typ pla val toMsg =
    input [ type_ typ, placeholder pla, value val, onInput toMsg ] []


viewTextArea : String -> String -> (String -> Msg) -> Html Msg
viewTextArea pla val toMsg =
    textarea [ placeholder pla, value val, onInput toMsg ] []


viewSelect : String -> String -> List String -> (String -> Msg) -> Html Msg
viewSelect pla val availableValues toMsg =
    div []
        [ label [] [ text (pla ++ ": ") ]
        , select [ value val, onInput toMsg ] (listToOptions availableValues)
        ]


viewSelectMultiple : String -> Model -> Html Msg
viewSelectMultiple sectionTitle model =
    div []
        [ h3 [] [ text (sectionTitle ++ ": ") ]
        , Html.map
            AddIssueSelectedDevicesChanged
          <|
            Multiselect.view model.availableDevices
        ]


listToOptions : List String -> List (Html Msg)
listToOptions data =
    let
        optionize x =
            option [ value x ] [ text x ]
    in
    List.map optionize data



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NothingYet _ ->
            ( model, Cmd.none )

        AddIssueIssueTrackerIDChanged id ->
            let
                addIssueInputs =
                    model.addIssueInputs

                newValue =
                    { addIssueInputs | issueTrackerID = id }
            in
            ( { model | addIssueInputs = newValue }, Cmd.none )

        AddIssuePriorityChanged priority ->
            let
                addIssueInputs =
                    model.addIssueInputs

                newValue =
                    { addIssueInputs | priority = priority }
            in
            ( { model | addIssueInputs = newValue }, Cmd.none )

        AddIssueSelectedDevicesChanged sub ->
            let
                ( subModel, subCmd, _ ) =
                    Multiselect.update sub model.availableDevices
            in
            ( { model | availableDevices = subModel }, Cmd.map AddIssueSelectedDevicesChanged subCmd )

        AddIssueIssueTypeChanged issueType ->
            let
                addIssueInputs =
                    model.addIssueInputs

                newValue =
                    { addIssueInputs | issueType = issueType }
            in
            ( { model | addIssueInputs = newValue }, Cmd.none )

        AddIssueSummaryChanged summary ->
            let
                addIssueInputs =
                    model.addIssueInputs

                newValue =
                    { addIssueInputs | summary = summary }
            in
            ( { model | addIssueInputs = newValue }, Cmd.none )

        AddIssueDescriptionChanged desc ->
            let
                addIssueInputs =
                    model.addIssueInputs

                newValue =
                    { addIssueInputs | description = desc }
            in
            ( { model | addIssueInputs = newValue }, Cmd.none )


type Msg
    = NothingYet String
    | AddIssueIssueTrackerIDChanged String
    | AddIssuePriorityChanged String
    | AddIssueSelectedDevicesChanged Multiselect.Msg
    | AddIssueIssueTypeChanged String
    | AddIssueSummaryChanged String
    | AddIssueDescriptionChanged String



-- MOCK DATA FROM THE SERVER


availableDeviceModels =
    -- non-deprecated models only
    [ { device_model_id = "1", name = "device model 1" }
    , { device_model_id = "2", name = "device model 2" }
    ]


availablePriorities =
    [ { priority_id = 1, name = "Low" }
    , { priority_id = 2, name = "Medium" }
    , { priority_id = 3, name = "High" }
    ]


availableIssueTypes =
    [ { issue_id = 1, name = "Product" }
    , { issue_id = 2, name = "Firmware" }
    , { issue_id = 3, name = "Test framework" }
    , { issue_id = 4, name = "Requires investigation" }
    ]



-- UTILS


linkToString : Link -> String
linkToString link =
    case link of
        IssueTrackerID str ->
            "IssueTracker link: " ++ str


priorityToString : Priority -> String
priorityToString priority =
    case priority of
        Low ->
            "lower depths of backlog"

        Medium ->
            "normal"

        High ->
            "devices on fire, fix ASAP"


bugTypeToString : BugType -> String
bugTypeToString bugType =
    case bugType of
        Product ->
            "Product bug"

        Firmware ->
            "Firmware bug"

        TestFramework ->
            "Test framework bug"

        NeedsInvestigation ->
            "Requires investigation"



-- MODEL


type Priority
    = Low
    | Medium
    | High


type Link
    = IssueTrackerID String


type BugType
    = Product
    | Firmware
    | TestFramework
    | NeedsInvestigation


type alias Issue =
    { id : Int
    , priority : Priority
    , link : Link
    , summary : String
    , description : Maybe String
    , bugType : BugType
    }


type alias Model =
    { issues : List Issue
    , addIssueInputs : AddIssueInputs
    , availableDevices : Multiselect.Model
    }


type alias AddIssueInputs =
    { issueTrackerID : String
    , priority : String
    , affectedDeviceModels : List String
    , issueType : String
    , summary : String
    , description : String
    }


valuesExample : List ( String, String )
valuesExample =
    List.map (\recordvalue -> ( recordvalue.device_model_id, recordvalue.name )) availableDeviceModels


initialModel : Model
initialModel =
    { issues =
        [ { id = 1
          , priority = Low
          , link = IssueTrackerID "A-1"
          , summary = "Things are broken"
          , description = Just "A detailed description why things are broken"
          , bugType = Firmware
          }
        , { id = 2
          , priority = High
          , link = IssueTrackerID "B-1"
          , summary = "Things are broken"
          , description = Just "A detailed description why things are broken"
          , bugType = TestFramework
          }
        , { id = 3
          , priority = High
          , link = IssueTrackerID "C-1"
          , summary = "Things are broken"
          , description = Nothing
          , bugType = NeedsInvestigation
          }
        , { id = 4
          , priority = Medium
          , link = IssueTrackerID "C-2"
          , summary = "Things are broken"
          , description = Just "A detailed description why things are broken"
          , bugType = Product
          }
        ]
    , addIssueInputs =
        { issueTrackerID = ""
        , priority = ""
        , affectedDeviceModels = List.map .name availableDeviceModels
        , issueType = ""
        , summary = ""
        , description = ""
        }
    , availableDevices = Multiselect.initModel valuesExample "available devices" Multiselect.Show
    }


init : ( Model, Cmd Msg )
init =
    ( initialModel, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Sub.map AddIssueSelectedDevicesChanged <| Multiselect.subscriptions model.availableDevices
        ]



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = \_ -> init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
