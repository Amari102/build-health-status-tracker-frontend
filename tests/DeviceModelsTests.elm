module DeviceModelsTests exposing (..)

import DeviceModels
import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Json.Decode exposing (decodeString, decodeValue)
import Json.Encode as Encode
import Test exposing (..)


test1 : Test
test1 =
    test "parse valid device model entry, without required fields"
        (\_ ->
            let
                jsonToTest =
                    """
                    {
                    "name": "Celestron Outland X 6x30",
                    "device_model_id": 42
                    }
                    """

                expectedDeviceModel =
                    { name = "Celestron Outland X 6x30", is_deprecated = False, device_model_id = 42 }
            in
            jsonToTest
                |> decodeString DeviceModels.deviceModelDecoder
                |> Expect.equal
                    (Ok expectedDeviceModel)
        )


test2 : Test
test2 =
    test "retrieve device model name from valid JSON, without required fields"
        (\_ ->
            let
                jsonToTest =
                    """
                    {
                    "device_model_id": 123
                    , "name": "Celestron Outland X 6x30"
                    }
                    """

                expectedName =
                    "Celestron Outland X 6x30"
            in
            jsonToTest
                |> decodeString DeviceModels.deviceModelDecoder
                -- |> Result.map (\deviceModel -> deviceModel.name)
                -- shorter syntax:
                |> Result.map .name
                |> Expect.equal
                    (Ok expectedName)
        )


test3 : Test
test3 =
    test "retrieve is_deprecated value from JSON, where it is set to False" <|
        \_ ->
            let
                jsonToTest =
                    """
                    {
                    "device_model_id": 456
                    , "name": "Celestron Outland X 6x30",
                    "is_deprecated" : false
                    }
                    """

                expectedValue =
                    False
            in
            jsonToTest
                |> decodeString DeviceModels.deviceModelDecoder
                |> Result.map .is_deprecated
                |> Expect.equal
                    (Ok expectedValue)


test4 : Test
test4 =
    test "retrieve is_deprecated value from JSON, where it is set to null" <|
        \_ ->
            let
                jsonToTest =
                    """
                    {
                    "device_model_id": 0
                    , "name": "Celestron Outland X 6x30",
                    "is_deprecated" : null
                    }
                    """

                expectedValue =
                    False
            in
            jsonToTest
                |> decodeString DeviceModels.deviceModelDecoder
                |> Result.map .is_deprecated
                |> Expect.equal
                    (Ok expectedValue)


test5 : Test
test5 =
    fuzz string "device model is not deprecated for any name if is_deprecated is not set" <|
        \name ->
            [ ( "name", Encode.string name ), ( "device_model_id", Encode.int 42 ) ]
                |> Encode.object
                |> decodeValue DeviceModels.deviceModelDecoder
                |> Result.map .is_deprecated
                |> Expect.equal (Ok False)


test51 : Test
test51 =
    fuzz int "device model is not deprecated for any device id if is_deprecated is not set" <|
        \device_model_id ->
            [ ( "name", Encode.string "Frobnicator 5000" ), ( "device_model_id", Encode.int device_model_id ) ]
                |> Encode.object
                |> decodeValue DeviceModels.deviceModelDecoder
                |> Result.map .is_deprecated
                |> Expect.equal (Ok False)


test6 : Test
test6 =
    test "decode full JSON from server containing device models info into device model names" <|
        let
            jsonToTest =
                """
                {
                "data": [
                    {   "device_model_id": 1
                        , "name": "Celestron Outland X 6x30"
                    },
                    {
                        "device_model_id": -1
                        , "name": "Celestron Outland X 6x30 Super Plus"
                    }
                ]
            }
            """

            expectedValues =
                [ "Celestron Outland X 6x30"
                , "Celestron Outland X 6x30 Super Plus"
                ]
        in
        \_ ->
            jsonToTest
                -- decode Json into (Ok <DeviceModelsData record>)
                |> decodeString DeviceModels.deviceModelsDataDecoder
                -- get list of records under "data" key in DeviceModelsData record
                |> Result.map .data
                -- map list of device model records to extract just their names
                |> Result.map (List.map .name)
                |> Expect.equal (Ok expectedValues)


test7 : Test
test7 =
    test "decode full JSON from server containing device models info into device model names, using improved direct decoder" <|
        let
            jsonToTest =
                """
                {
                "data": [
                    {
                        "device_model_id": 123
                        , "name": "Celestron Outland X 6x30"
                    },
                    {
                        "device_model_id": 123
                        , "name": "Celestron Outland X 6x30 Super Plus"
                    }
                ]
            }
            """

            expectedValues =
                [ "Celestron Outland X 6x30"
                , "Celestron Outland X 6x30 Super Plus"
                ]
        in
        \_ ->
            jsonToTest
                -- decode Json into (Ok <list of device models>)
                |> decodeString DeviceModels.directDeviceModelsDecoder
                |> Result.map (List.map .name)
                |> Expect.equal (Ok expectedValues)
